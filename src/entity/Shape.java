package entity;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import game.GameState;
import constants.Constants;

public class Shape {
    private List<Square> squares;
    private int type;
    private int x, y;
    private Color color;
    private boolean move = true;
    private int position = 1;

    public Shape(int type, Color color, int x, int y) {
        this.type = type;
        this.color = color;
        this.x = x;
        this.y = y;
        squares = new ArrayList<>();
        switch (type) {
            case Constants.WOOD:
                squares.add(new Square(new Vector2D(x, y), color));
                squares.add(new Square(new Vector2D(x, y - 1), color));
                squares.add(new Square(new Vector2D(x, y - 2), color));
                squares.add(new Square(new Vector2D(x, y - 3), color));
                break;
            case Constants.LEFT_FOOD:
                squares.add(new Square(new Vector2D(x, y), color));
                squares.add(new Square(new Vector2D(x + 1, y), color));
                squares.add(new Square(new Vector2D(x + 1, y - 1), color));
                squares.add(new Square(new Vector2D(x + 1, y - 2), color));
                break;
            case Constants.RIGHT_FOOD:
                squares.add(new Square(new Vector2D(x, y), color));
                squares.add(new Square(new Vector2D(x - 1, y), color));
                squares.add(new Square(new Vector2D(x - 1, y - 1), color));
                squares.add(new Square(new Vector2D(x - 1, y - 2), color));
                break;
            case Constants.WINDOW:
                squares.add(new Square(new Vector2D(x, y), color));
                squares.add(new Square(new Vector2D(x, y - 1), color));
                squares.add(new Square(new Vector2D(x + 1, y), color));
                squares.add(new Square(new Vector2D(x + 1, y - 1), color));
                break;
            case Constants.MOUNTAIN:
                squares.add(new Square(new Vector2D(x, y), color));
                squares.add(new Square(new Vector2D(x + 1, y), color));
                squares.add(new Square(new Vector2D(x + 2, y), color));
                squares.add(new Square(new Vector2D(x + 1, y - 1), color));
                break;
            case Constants.RIGHT_DUCK:
                squares.add(new Square(new Vector2D(x, y), color));
                squares.add(new Square(new Vector2D(x + 1, y), color));
                squares.add(new Square(new Vector2D(x + 1, y - 1), color));
                squares.add(new Square(new Vector2D(x + 2, y - 1), color));
                break;
            case Constants.LEFT_DUCK:
                squares.add(new Square(new Vector2D(x, y), color));
                squares.add(new Square(new Vector2D(x + 1, y), color));
                squares.add(new Square(new Vector2D(x, y - 1), color));
                squares.add(new Square(new Vector2D(x - 1, y - 1), color));
                break;
        }
    }

    public Shape() {
    }

    public List<Square> getSquares() {
        return squares;
    }

    public static Shape getRandomShape() {
        Random random = new Random(System.nanoTime());
        int type = random.nextInt(7) + 1;
        Color color = Color.getHSBColor(random.nextInt(255), random.nextInt(255), random.nextInt(255));
        return new Shape(type, color, 5, -1);
    }

    public boolean isMove() {
        return move;
    }

    public void setMove(boolean move) {
        this.move = move;
    }

    public int getType() {
        return type;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public static int getMaxSquareX(Shape shape) {
        int Max = 0;
        for (Square square : shape.getSquares()) {
            if (square.getVector2D().getX() > Max) {
                Max = square.getVector2D().getX();
            }
        }
        return Max;
    }

    public static int getMinSquareX(Shape shape) {
        int Min = shape.getSquares().get(0).getVector2D().getX();
        for (Square square : shape.getSquares()) {
            if (square.getVector2D().getX() < Min) {
                Min = square.getVector2D().getX();
            }
        }
        return Min;
    }

    public static String isCrash(Shape shape) {
        for (Square randomShapeSquare : shape.getSquares()) {
            for (Square square : GameState.getInstance().getSquares()) {
                if (square.getVector2D().getX() != -1 && square.getVector2D().getX() != Constants.n) {
                    if (Square.getBox2(square).intersects(Square.getBox2(randomShapeSquare))) {
                        if (square.getVector2D().getX() <= randomShapeSquare.getVector2D().getX()) {
                            return "left is illegal";
                        } else
                            return "right is illegal";
                    }
                }
            }
        }
        return null;
    }

    public static boolean checkPositionOfRandomShape(Shape randomShape) {
        if (randomShape.type == Constants.WOOD) {
            return !GameState.isExist(5, 0);
        } else if (randomShape.type == Constants.LEFT_DUCK || randomShape.type == Constants.LEFT_FOOD ||
                randomShape.type == Constants.RIGHT_DUCK || randomShape.type == Constants.RIGHT_FOOD ||
                randomShape.type == Constants.WINDOW) {
            if (GameState.isExist(5, 0) || GameState.isExist(6, 0)) return false;
        } else if (randomShape.type == Constants.MOUNTAIN) {
            if (GameState.isExist(5, 0) || GameState.isExist(6, 0) || GameState.isExist(7, 0)) return false;
        }
        return true;
    }

    public Square getSquare(int i, int j) {
        for (Square square : this.getSquares()) {
            if (square.getVector2D().getX() == i && square.getVector2D().getY() == j) {
                return square;
            }
        }
        return null;
    }
}
