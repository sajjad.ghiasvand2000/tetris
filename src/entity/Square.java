package entity;

import java.awt.*;

public class Square {
    private Vector2D vector2D;
    private Color color;

    public Square(Vector2D vector2D, Color color) {
        this.vector2D = vector2D;
        this.color = color;
    }

    public Vector2D getVector2D() {
        return vector2D;
    }

    public Color getColor() {
        return color;
    }

    public static Rectangle getBox1(Square square) {
        return new Rectangle(100 + 30 * square.getVector2D().getX(), 100 + 30 * square.getVector2D().getY(), 29, 31);
    }

    static Rectangle getBox2(Square square) {
        return new Rectangle(100 + 30 * square.getVector2D().getX(), 100 + 30 * square.getVector2D().getY(), 32, 29);
    }
}
