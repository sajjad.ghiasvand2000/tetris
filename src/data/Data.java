package data;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import entity.Square;
import game.GameState;

public class Data {

    public void SaveScore(int score) throws IOException {
        FileWriter fileWriter = new FileWriter("score.txt", true);
        fileWriter.write("" + score + "\r\n");
        fileWriter.flush();
        fileWriter.close();
    }

    public int[] findTopTenScores() throws IOException {
        int[] array = new int[10];
        for (int i = 0; i < 10; i++) {
            Scanner fileReader = new Scanner(new FileReader("score.txt"));
            int Max = 0;
            while (fileReader.hasNext()) {
                boolean flag = false;
                String s = fileReader.nextLine();
                int x = Integer.parseInt(s);
                if (x > Max) {
                    for (int j = 0; j < i; j++) {
                        if (array[j] <= x) {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag)
                        Max = x;
                }
            }
            fileReader.close();
            array[i] = Max;
        }
        return array;
    }

    public static void saveGameState() throws IOException {
        FileWriter fileWriter = new FileWriter("tetris.txt", true);
        for (Square square : GameState.getInstance().getSquares()) {
            String data = new Gson().toJson(square);
            fileWriter.write(data + "\r\n");
            fileWriter.flush();
        }
        fileWriter.close();

        FileWriter fileWriter1 = new FileWriter("score and line.txt");
        fileWriter1.write("" + GameState.getScore() + "\r\n");
        fileWriter1.write(GameState.getDeletedLine() + "");
        fileWriter1.flush();
        fileWriter1.close();
    }

    public static void setGameState() throws FileNotFoundException {
        Scanner fileReader = new Scanner(new FileReader("tetris.txt"));
        List<Square> squares = new ArrayList<>();
        while (fileReader.hasNext()) {
            Square square = new Gson().fromJson(fileReader.nextLine(), Square.class);
            squares.add(square);
        }
        GameState.getInstance().setSquares(squares);
        Scanner fileReader1 = new Scanner(new FileReader("score and line.txt"));
        String score = fileReader1.nextLine();
        GameState.setScore(Integer.parseInt(score));
        String deletedLine = fileReader1.nextLine();
        GameState.setDeletedLine(Integer.parseInt(deletedLine));
    }

    public static void clearFiles() {
        File file = new File("tetris.txt");
        file.delete();
        File file1 = new File("score and line.txt");
        file1.delete();
    }
}
