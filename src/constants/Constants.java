package constants;

public class Constants {
    public final static int WIDTH = 700;
    public final static int HEIGHT = 800;
    public final static int DELAY = 500;
    public final static int START_WIDTH_BOARD = 100;
    public final static int START_HEIGHT_BOARD = 100;
    public final static int WIDTH_OF_BOARD = 300;
    public final static int HEIGHT_OF_BOARD = 600;
    public final static int WIDTH_OF_BACKGROUND_BORDER = 4;
    public final static int WOOD = 1;
    public final static int LEFT_FOOD = 2;
    public final static int RIGHT_FOOD = 3;
    public final static int WINDOW = 4;
    public final static int MOUNTAIN = 5;
    public final static int LEFT_DUCK = 6;
    public final static int RIGHT_DUCK = 7;
    public final static int m = 20;
    public final static int n = 10;
}
