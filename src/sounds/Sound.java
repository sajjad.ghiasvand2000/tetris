package sounds;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import game.GameState;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Sound {

    static void playMusic(String filePath) throws FileNotFoundException, JavaLayerException {
        FileInputStream fileInputStream = new FileInputStream(filePath);
        Player player = new Player(fileInputStream);
        player.play();
    }

    public static void soundOfDeletedRow(){
        if (GameState.getDeletedLine() == 1){
            new SoundPlayer("sounds////VO_EXLNT.mp3");
        } else if (GameState.getDeletedLine() == 2){
            new SoundPlayer("sounds////VO_FANTSTC.mp3");
        } else if (GameState.getDeletedLine() == 3){
            new SoundPlayer("sounds////VO_WONDRFL.mp3");
        }else if (GameState.getDeletedLine() >= 4){
            new SoundPlayer("sounds////VO_WOW.mp3");
        }
    }

}
