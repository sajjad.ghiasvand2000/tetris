package sounds;

import javazoom.jl.decoder.JavaLayerException;
import java.io.FileNotFoundException;

public class SoundPlayer extends Thread {
    public SoundPlayer(String fileName){
        new Thread(() -> {
            try {
                Sound.playMusic(fileName);
            } catch (FileNotFoundException | JavaLayerException e) {
                e.printStackTrace();
            }
        }).start();
    }
    Thread thread = new Thread();

}
