package display;

import game.GameState;

import javax.swing.*;
import java.io.File;

public class QuestionAtFirst {

    public QuestionAtFirst() {

        String[] options = new String[]{"Yes", "No"};
        File file = new File("tetris.txt");
        if (file.exists()) {
            int result = JOptionPane.showOptionDialog(
                    null,
                    "Do you want to continue the previous game?",
                    "Option Dialog",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null,
                    options,
                    options[0]
            );
            if (result == 0)
                GameState.setPastGame(true);
            else if (result == 1)
                GameState.setPastGame(false);
        }
    }
}
