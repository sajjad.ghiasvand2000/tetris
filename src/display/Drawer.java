package display;

import entity.Shape;
import entity.Square;
import game.GameState;
import constants.*;
import java.awt.*;

public class Drawer {
    private Graphics2D graphics2D;

    public Drawer(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
    }

    public void drawBackground() {
        graphics2D.setColor(Color.black);
        graphics2D.fillRect(Constants.START_WIDTH_BOARD, Constants.START_HEIGHT_BOARD,
                Constants.WIDTH_OF_BOARD, Constants.HEIGHT_OF_BOARD);
    }

    public void drawBackgroundBorder() {
        graphics2D.setColor(Color.CYAN);
        graphics2D.fillRect(Constants.START_WIDTH_BOARD - Constants.WIDTH_OF_BACKGROUND_BORDER,
                Constants.START_HEIGHT_BOARD - Constants.WIDTH_OF_BACKGROUND_BORDER,
                Constants.WIDTH_OF_BOARD + 2 * Constants.WIDTH_OF_BACKGROUND_BORDER,
                Constants.HEIGHT_OF_BOARD + 2 * Constants.WIDTH_OF_BACKGROUND_BORDER);
    }

    public void drawBackgroundNext() {
        graphics2D.setColor(Color.black);
        graphics2D.fillRect(450, 240,
                150, 170);
    }

    public void drawBackgroundBorderNext() {
        graphics2D.setColor(Color.CYAN);
        graphics2D.fillRect(446, 236,
                158, 178);
    }

    public void drawBackgroundScore() {
        graphics2D.setColor(Color.black);
        graphics2D.fillRect(450, 40,
                150, 160);
    }

    public void drawBackgroundBorderScore() {
        graphics2D.setColor(Color.CYAN);
        graphics2D.fillRect(446, 36,
                158, 168);
    }

    public void drawBackgroundBestScores() {
        graphics2D.setColor(Color.black);
        graphics2D.fillRect(450, 450,
                150, 250);
    }

    public void drawBackgroundBorderBestScores() {
        graphics2D.setColor(Color.CYAN);
        graphics2D.fillRect(446, 446,
                158, 258);
    }

    public void drawBackgroundDeletedLines(){
        graphics2D.setColor(Color.black);
        graphics2D.fillRect(100, 40,
                300, 40);
    }

    public void drawBackgroundBorderDeletedLines(){
        graphics2D.setColor(Color.CYAN);
        graphics2D.fillRect(96, 36,
                308, 48);
    }


    private void drawSquare(Square square) {
        graphics2D.setColor(square.getColor());
        graphics2D.fillRect(101 + 30 * square.getVector2D().getX(), 101 + 30 * square.getVector2D().getY(), 28, 28);
    }

    public void drawShape(entity.Shape shape) {
        for (Square square : shape.getSquares()) {
            if (square.getVector2D().getY() >= 0) {
                drawSquare(square);
            }
        }
    }

    public void drawGameState() {
        for (Square square : GameState.getInstance().getSquares()) {
            if (square.getVector2D().getY() != Constants.m && square.getVector2D().getY() >= 0
                    && square.getVector2D().getX() >= 0 && square.getVector2D().getX() != Constants.n) {
                drawSquare(square);
            }
        }
    }

    public void drawNextShape(entity.Shape shape){
        entity.Shape AbstractShape = new Shape(shape.getType(), shape.getColor(),13,9);
        for (Square square : AbstractShape.getSquares()) {
            drawSquare(square);
        }
    }

    public void drawNextString(){
        String prompt = "NEXT";
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        graphics2D.setColor(Color.GREEN);
        graphics2D.setFont(font);
        graphics2D.drawString(prompt , 450 + (150 - width)/2 , 270);
    }

    public void drawScoreString(){
        String prompt = "SCORE";
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        graphics2D.setColor(Color.GREEN);
        graphics2D.setFont(font);
        graphics2D.drawString(prompt , 450 + (150 - width)/2 , 65);
    }

    public void drawScoreNumber(){
        String prompt = ""+ GameState.getScore();
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        graphics2D.setColor(Color.BLUE);
        graphics2D.setFont(font);
        graphics2D.drawString(prompt , 450 + (150 - width)/2 , 90);
    }

    public void drawBestScoreString(){
        String prompt = "BEST SCORES";
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        graphics2D.setColor(Color.GREEN);
        graphics2D.setFont(font);
        graphics2D.drawString(prompt , 450 + (150 - width)/2 , 480);
    }

    public void drawBestScoresNumber(int[] scores) {
        for (int i = 0; i < scores.length; i++) {
            String prompt = "" + scores[i];
            Font font = new Font("Helvetica", Font.BOLD, 15);
            FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
            int width = fontMetrics.stringWidth(prompt);
            graphics2D.setColor(Color.BLUE);
            graphics2D.setFont(font);
            graphics2D.drawString(prompt, 450 + (150 - width) / 2, 505 + 20*i);
        }
    }

    public void drawNumberOfDeletedLine(){
        String prompt = "LINES : " + GameState.getDeletedLine();
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        graphics2D.setColor(Color.GREEN);
        graphics2D.setFont(font);
        graphics2D.drawString(prompt , 100 + (300 - width)/2 , 65);
    }

    public void drawStartAndEnd(String prompt){
        Font font = new Font("Helvetica", Font.BOLD, 50);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        graphics2D.setColor(Color.RED);
        graphics2D.setFont(font);
        graphics2D.drawString(prompt , (700-width)/2 , (800-20)/2);
    }



}