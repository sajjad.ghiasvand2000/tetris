import constants.Constants;
import data.Data;
import game.GamePlay;
import game.GameState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        JFrame frame = new JFrame();
        GamePlay gamePlay = new GamePlay();
        frame.getContentPane().setBackground(Color.DARK_GRAY);
        frame.setTitle("Tetris");
        frame.setSize(Constants.WIDTH, Constants.HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(gamePlay);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    if (!GameState.isEndGame())
                        Data.saveGameState();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
