package game;

import entity.Shape;

class CheckSpace {

    static boolean mountain(Shape shape, int position, boolean rightOrLeft) {
        int x = shape.getX();
        int y = shape.getY();
        if (position == 1 && rightOrLeft) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x + 2, y - 1) &&
                    !GameState.isExist(x + 2, y + 1) && !GameState.isExist(x + 1, y + 1)) {
                shape.getSquare(x, y).getVector2D().addVector(1, 1);
                return true;
            }
        } else if (position == 1) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x + 2, y - 1) &&
                    !GameState.isExist(x, y + 1) && !GameState.isExist(x + 1, y + 1)) {
                shape.getSquare(x + 2, y).getVector2D().addVector(-1, 1);
                return true;
            }
        } else if (position == 2 && rightOrLeft) {
            if (!GameState.isExist(x, y) && !GameState.isExist(x + 2, y - 1) &&
                    !GameState.isExist(x + 2, y + 1) && !GameState.isExist(x, y + 1)) {
                shape.getSquare(x + 1, y - 1).getVector2D().addVector(-1, 1);
                return true;
            }
        } else if (position == 2) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x + 2, y - 1) &&
                    !GameState.isExist(x + 2, y + 1) && !GameState.isExist(x, y)) {
                shape.getSquare(x + 1, y + 1).getVector2D().addVector(-1, -1);
                return true;
            }
        } else if (position == 3 && rightOrLeft) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x + 1, y - 1) &&
                    !GameState.isExist(x, y + 1) && !GameState.isExist(x + 2, y + 1)) {
                shape.getSquare(x + 2, y).getVector2D().addVector(-1, -1);
                return true;
            }
        } else if (position == 3) {
            if (!GameState.isExist(x + 1, y - 1) && !GameState.isExist(x + 2, y - 1) &&
                    !GameState.isExist(x + 2, y + 1) && !GameState.isExist(x, y + 1)) {
                shape.getSquare(x, y).getVector2D().addVector(1, -1);
                return true;
            }
        } else if (position == 4 && rightOrLeft) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x, y + 1) &&
                    !GameState.isExist(x + 2, y - 1) && !GameState.isExist(x + 2, y)) {
                shape.getSquare(x + 1, y + 1).getVector2D().addVector(1, -1);
                return true;
            }
        } else if (position == 4) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x, y + 1) &&
                    !GameState.isExist(x + 2, y) && !GameState.isExist(x + 2, y + 1)) {
                shape.getSquare(x + 1, y - 1).getVector2D().addVector(1, 1);
                return true;
            }
        }
        return false;
    }

    static boolean rightFood(Shape shape, int position, boolean rightOrLeft) {
        int x = shape.getX();
        int y = shape.getY();
        if (position == 1 && rightOrLeft) {
            if (!GameState.isExist(x, y - 2) && !GameState.isExist(x, y - 1) &&
                    !GameState.isExist(x - 2, y - 1) && !GameState.isExist(x - 2, y)) {
                shape.getSquare(x - 1, y - 2).getVector2D().addVector(1, 1);
                shape.getSquare(x - 1, y).getVector2D().addVector(-1, 0);
                shape.getSquare(x, y).getVector2D().addVector(-2, -1);
                return true;
            }
        } else if (position == 1) {
            if (!GameState.isExist(x - 2, y - 2) && !GameState.isExist(x - 2, y - 1) &&
                    !GameState.isExist(x, y - 2) && !GameState.isExist(x, y - 1)) {
                shape.getSquare(x - 1, y - 2).getVector2D().addVector(-1, 1);
                shape.getSquare(x, y).getVector2D().addVector(0, -1);
                shape.getSquare(x - 1, y).getVector2D().addVector(1, -2);
                return true;
            }
        } else if (position == 2 && rightOrLeft) {
            if (!GameState.isExist(x - 2, y - 2) && !GameState.isExist(x - 1, y - 2) &&
                    !GameState.isExist(x - 1, y) && !GameState.isExist(x, y)) {
                shape.getSquare(x, y - 1).getVector2D().addVector(-1, 1);
                shape.getSquare(x - 2, y - 1).getVector2D().addVector(0, -1);
                shape.getSquare(x - 2, y).getVector2D().addVector(1, -2);
                return true;
            }
        } else if (position == 2) {
            if (!GameState.isExist(x - 1, y - 2) && !GameState.isExist(x, y - 2) &&
                    !GameState.isExist(x - 1, y) && !GameState.isExist(x, y)) {
                shape.getSquare(x, y - 1).getVector2D().addVector(-1, -1);
                shape.getSquare(x - 2, y).getVector2D().addVector(1, 0);
                shape.getSquare(x - 2, y - 1).getVector2D().addVector(2, 1);
                return true;
            }
        } else if (position == 3 && rightOrLeft) {
            if (!GameState.isExist(x - 2, y - 1) && !GameState.isExist(x - 2, y) &&
                    !GameState.isExist(x, y - 2) && !GameState.isExist(x, y - 1)) {
                shape.getSquare(x - 1, y).getVector2D().addVector(-1, -1);
                shape.getSquare(x - 1, y - 2).getVector2D().addVector(1, 0);
                shape.getSquare(x - 2, y - 2).getVector2D().addVector(2, 1);
                return true;
            }
        } else if (position == 3) {
            if (!GameState.isExist(x - 2, y - 1) && !GameState.isExist(x - 2, y) &&
                    !GameState.isExist(x, y - 1) && !GameState.isExist(x, y)) {
                shape.getSquare(x - 1, y).getVector2D().addVector(1, -1);
                shape.getSquare(x - 2, y - 2).getVector2D().addVector(0, 1);
                shape.getSquare(x - 1, y - 2).getVector2D().addVector(-1, 2);
                return true;
            }
        } else if (position == 4 && rightOrLeft) {
            if (!GameState.isExist(x - 2, y - 2) && !GameState.isExist(x - 1, y - 2) &&
                    !GameState.isExist(x - 1, y) && !GameState.isExist(x, y)) {
                shape.getSquare(x - 2, y - 1).getVector2D().addVector(1, -1);
                shape.getSquare(x, y - 1).getVector2D().addVector(0, 1);
                shape.getSquare(x, y - 2).getVector2D().addVector(-1, 2);
                return true;
            }
        } else if (position == 4) {
            if (!GameState.isExist(x - 2, y - 2) && !GameState.isExist(x - 1, y - 2) &&
                    !GameState.isExist(x - 2, y) && !GameState.isExist(x - 1, y)) {
                shape.getSquare(x - 2, y - 1).getVector2D().addVector(1, 1);
                shape.getSquare(x, y - 2).getVector2D().addVector(-1, 0);
                shape.getSquare(x, y - 1).getVector2D().addVector(-2, -1);
                return true;
            }
        }
        return false;
    }

    static boolean leftFood(Shape shape, int position, boolean rightOrLeft) {
        int x = shape.getX();
        int y = shape.getY();
        if (position == 1 && rightOrLeft) {
            if (!GameState.isExist(x, y - 2) && !GameState.isExist(x, y - 1) &&
                    !GameState.isExist(x + 2, y - 2) && !GameState.isExist(x + 2, y - 1)) {
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(1, 1);
                shape.getSquare(x, y).getVector2D().addVector(0, -1);
                shape.getSquare(x + 1, y).getVector2D().addVector(-1, -2);
                return true;
            }
        } else if (position == 1) {
            if (!GameState.isExist(x, y - 2) && !GameState.isExist(x, y - 1) &&
                    !GameState.isExist(x + 2, y - 1) && !GameState.isExist(x + 2, y)) {
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(-1, 1);
                shape.getSquare(x + 1, y).getVector2D().addVector(1, 0);
                shape.getSquare(x, y).getVector2D().addVector(2, -1);
                return true;
            }
        } else if (position == 2 && rightOrLeft) {
            if (!GameState.isExist(x + 1, y - 2) && !GameState.isExist(x + 2, y - 2) &&
                    !GameState.isExist(x + 1, y) && !GameState.isExist(x + 2, y)) {
                shape.getSquare(x + 2, y - 1).getVector2D().addVector(-1, 1);
                shape.getSquare(x, y - 2).getVector2D().addVector(1, 0);
                shape.getSquare(x, y - 1).getVector2D().addVector(2, -1);
                return true;
            }
        } else if (position == 2) {
            if (!GameState.isExist(x + 1, y - 2) && !GameState.isExist(x + 2, y - 2) &&
                    !GameState.isExist(x, y) && !GameState.isExist(x + 1, y)) {
                shape.getSquare(x + 2, y - 1).getVector2D().addVector(-1, -1);
                shape.getSquare(x, y - 1).getVector2D().addVector(0, 1);
                shape.getSquare(x, y - 2).getVector2D().addVector(1, 2);
                return true;
            }
        } else if (position == 3 && rightOrLeft) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x, y) &&
                    !GameState.isExist(x + 2, y - 1) && !GameState.isExist(x + 2, y)) {
                shape.getSquare(x + 1, y).getVector2D().addVector(-1, -1);
                shape.getSquare(x + 2, y - 2).getVector2D().addVector(0, 1);
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(1, 2);
                return true;
            }
        } else if (position == 3) {
            if (!GameState.isExist(x, y - 2) && !GameState.isExist(x, y - 1) &&
                    !GameState.isExist(x + 2, y - 1) && !GameState.isExist(x + 2, y)) {
                shape.getSquare(x + 1, y).getVector2D().addVector(1, -1);
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(-1, 0);
                shape.getSquare(x + 2, y - 2).getVector2D().addVector(-2, 1);
                return true;
            }
        } else if (position == 4 && rightOrLeft) {
            if (!GameState.isExist(x, y - 2) && !GameState.isExist(x + 1, y - 2) &&
                    !GameState.isExist(x, y) && !GameState.isExist(x + 1, y)) {
                shape.getSquare(x, y - 1).getVector2D().addVector(1, -1);
                shape.getSquare(x + 2, y).getVector2D().addVector(-1, 0);
                shape.getSquare(x + 2, y - 1).getVector2D().addVector(-2, 1);
                return true;
            }
        } else if (position == 4) {
            if (!GameState.isExist(x + 1, y - 2) && !GameState.isExist(x + 2, y - 2) &&
                    !GameState.isExist(x, y) && !GameState.isExist(x + 1, y)) {
                shape.getSquare(x, y - 1).getVector2D().addVector(1, 1);
                shape.getSquare(x + 2, y - 1).getVector2D().addVector(0, -1);
                shape.getSquare(x + 2, y).getVector2D().addVector(-1, -2);
                return true;
            }
        }
        return false;
    }

    static boolean rightDuck(Shape shape, int position, boolean rightOrLeft) {
        int x = shape.getX();
        int y = shape.getY();
        if (position == 1 && rightOrLeft) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x + 1, y - 2) &&
                    !GameState.isExist(x + 2, y)) {
                shape.getSquare(x + 1, y).getVector2D().addVector(1, 0);
                shape.getSquare(x, y).getVector2D().addVector(1, -2);
                return true;
            }
        } else if (position == 1) {
            if (!GameState.isExist(x + 1, y - 2) && !GameState.isExist(x + 2, y - 2) &&
                    !GameState.isExist(x + 2, y)) {
                shape.getSquare(x + 1, y).getVector2D().addVector(1, 0);
                shape.getSquare(x, y).getVector2D().addVector(1, -2);
                return true;
            }
        } else if (position == 2 && rightOrLeft) {
            if (!GameState.isExist(x + 2, y - 2) && !GameState.isExist(x + 1, y) &&
                    !GameState.isExist(x, y)) {
                shape.getSquare(x + 2, y).getVector2D().addVector(-1, 0);
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(-1, 2);
                return true;
            }
        } else if (position == 2) {
            if (!GameState.isExist(x, y - 1) && !GameState.isExist(x, y) &&
                    !GameState.isExist(x + 1, y)) {
                shape.getSquare(x + 2, y).getVector2D().addVector(-1, 0);
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(-1, 2);
                return true;
            }
        }
        return false;
    }

    static boolean leftDuck(Shape shape, int position, boolean rightOrLeft) {
        int x = shape.getX();
        int y = shape.getY();
        if (position == 1 && rightOrLeft) {
            if (!GameState.isExist(x - 1, y - 2) && !GameState.isExist(x, y - 2) &&
                    !GameState.isExist(x - 1, y)) {
                shape.getSquare(x, y).getVector2D().addVector(-1, 0);
                shape.getSquare(x + 1, y).getVector2D().addVector(-1, -2);
                return true;
            }
        } else if (position == 1) {
            if (!GameState.isExist(x - 1, y) && !GameState.isExist(x, y - 2) &&
                    !GameState.isExist(x + 1, y - 1)) {
                shape.getSquare(x, y).getVector2D().addVector(-1, 0);
                shape.getSquare(x + 1, y).getVector2D().addVector(-1, -2);
                return true;
            }
        } else if (position == 2 && rightOrLeft) {
            if (!GameState.isExist(x + 1, y - 1) && !GameState.isExist(x + 1, y) &&
                    !GameState.isExist(x, y)) {
                shape.getSquare(x - 1, y).getVector2D().addVector(1, 0);
                shape.getSquare(x, y - 2).getVector2D().addVector(1, 2);
                return true;
            }
        } else if (position == 2) {
            if (!GameState.isExist(x - 1, y - 2) && !GameState.isExist(x, y) &&
                    !GameState.isExist(x + 1, y)) {
                shape.getSquare(x - 1, y).getVector2D().addVector(1, 0);
                shape.getSquare(x, y - 2).getVector2D().addVector(1, 2);
                return true;
            }
        }
        return false;
    }

    static boolean wood(Shape shape, int position, boolean rightOrLeft) {
        int x = shape.getX();
        int y = shape.getY();
        if (position == 1 && rightOrLeft) {
            if (!GameState.isExist(x - 2, y - 2) && !GameState.isExist(x - 1, y - 2) &&
                    !GameState.isExist(x - 2, y - 1) && !GameState.isExist(x - 1, y - 1)
                    && !GameState.isExist(x - 2, y) && !GameState.isExist(x - 1, y)
                    && !GameState.isExist(x + 1, y - 3) && !GameState.isExist(x + 1, y - 2)) {
                shape.getSquare(x, y - 3).getVector2D().addVector(1, 1);
                shape.getSquare(x, y - 1).getVector2D().addVector(-1, -1);
                shape.getSquare(x, y).getVector2D().addVector(-2, -2);
                return true;
            }
        } else if (position == 1) {
            if (!GameState.isExist(x - 1, y - 3) && !GameState.isExist(x - 1, y - 2) &&
                    !GameState.isExist(x + 1, y - 2) && !GameState.isExist(x - 2, y - 2)
                    && !GameState.isExist(x + 1, y - 1)
                    && !GameState.isExist(x + 1, y)) {
                shape.getSquare(x, y - 3).getVector2D().addVector(-1, 1);
                shape.getSquare(x, y - 1).getVector2D().addVector(1, -1);
                shape.getSquare(x, y).getVector2D().addVector(-2, -2);
                return true;
            }
        } else if (position == 2 && rightOrLeft) {
            if (!GameState.isExist(x - 1, y - 3) && !GameState.isExist(x - 2, y - 3) &&
                    !GameState.isExist(x, y - 3) && !GameState.isExist(x + 1, y - 1)
                    && !GameState.isExist(x, y - 1)
                    && !GameState.isExist(x, y)) {
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(-1, -1);
                shape.getSquare(x - 1, y - 2).getVector2D().addVector(1, 1);
                shape.getSquare(x - 2, y - 2).getVector2D().addVector(2, 2);
                return true;
            }
        } else if (position == 2) {
            if (!GameState.isExist(x - 2, y - 1) && !GameState.isExist(x - 1, y - 1) &&
                    !GameState.isExist(x, y - 1) && !GameState.isExist(x - 2, y)
                    && !GameState.isExist(x - 1, y) && !GameState.isExist(x, y)
                    && !GameState.isExist(x, y - 3) && !GameState.isExist(x + 1, y - 3)) {
                shape.getSquare(x + 1, y - 2).getVector2D().addVector(-1, -1);
                shape.getSquare(x - 1, y - 2).getVector2D().addVector(1, 1);
                shape.getSquare(x - 2, y - 2).getVector2D().addVector(2, 2);
                return true;
            }
        }
        return false;
    }
}