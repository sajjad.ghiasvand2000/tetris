package game;

import entity.Square;
import entity.Vector2D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import constants.Constants;

public class GameState {
    private List<Square> squares;
    private static GameState instance = new GameState();
    private static boolean play = false;
    private static int score = 0;
    private static int deletedLine = 0;
    private static boolean showNextShape = true;
    private static boolean back = false;
    private static boolean pastGame = false;
    private static boolean showPressEnter = false;
    private static boolean endGame = false;

    private GameState() {
        squares = new ArrayList<>();
        for (int i = 0; i < Constants.n; i++) {
            squares.add(new Square(new Vector2D(i, Constants.m), Color.BLUE));
        }
        for (int i = 0; i < Constants.m; i++) {
            squares.add(new Square(new Vector2D(-1, i), Color.BLUE));
        }

        for (int i = 0; i < Constants.m; i++) {
            squares.add(new Square(new Vector2D(Constants.n, i), Color.BLUE));
        }
    }

    public static GameState getInstance() {
        return instance;
    }

    public List<Square> getSquares() {
        return squares;
    }

    public void setSquares(List<Square> squares) {
        this.squares = squares;
    }

    public void addSquare(Square square) {
        squares.add(square);
    }

    public void removeSquare(Square square) {
        squares.remove(square);
    }

    static boolean isPlay() {
        return play;
    }

    public static void setPlay(boolean play) {
        GameState.play = play;
    }

    public static int getScore() {
        return score;
    }

    public static void setScore(int score) {
        GameState.score = score;
    }

    public static boolean isPastGame() {
        return pastGame;
    }

    public static void setPastGame(boolean pastGame) {
        GameState.pastGame = pastGame;
    }

    public static boolean isBack() {
        return back;
    }

    public static void setBack(boolean back) {
        GameState.back = back;
    }

    public static boolean isShowNextShape() {
        return showNextShape;
    }

    public static void setShowNextShape(boolean showNextShape) {
        GameState.showNextShape = showNextShape;
    }

    public static int getDeletedLine() {
        return deletedLine;
    }

    public static void setDeletedLine(int deletedLine) {
        GameState.deletedLine = deletedLine;
    }

    public static boolean isShowPressEnter() {
        return showPressEnter;
    }

    public static void setShowPressEnter(boolean showPressEnter) {
        GameState.showPressEnter = true;
    }

    public static boolean isEndGame() {
        return endGame;
    }

    public static void setEndGame(boolean endGame) {
        GameState.endGame = endGame;
    }

    public static boolean isExist(int i, int j) {
        for (Square square : instance.getSquares()) {
            if (square.getVector2D().getX() == i && square.getVector2D().getY() == j) {
                return true;
            }
        }
        return false;
    }

    static Square getSquare(int i, int j) {
        for (Square square : instance.getSquares()) {
            if (square.getVector2D().getX() == i && square.getVector2D().getY() == j) {
                return square;
            }
        }
        return null;
    }
}
