package game;

import entity.Shape;
import entity.Square;
import data.*;
import constants.Constants;
import sounds.SoundPlayer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

public class KeyboardListener implements KeyListener {
    private Shape shape;

    KeyboardListener(Shape shape) {
        this.shape = shape;
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        String flag = Shape.isCrash(shape);
        if (shape.isMove()) {
            switch (keyEvent.getKeyCode()) {
                case KeyEvent.VK_RIGHT:
                    int Max = Shape.getMaxSquareX(shape);
                    if (Max < 9 && (flag == null || flag.equals("left is illegal"))) {
                        for (Square square : shape.getSquares()) {
                            square.getVector2D().addX(1);
                        }
                        shape.setX(shape.getX() + 1);
                    }
                    break;
                case KeyEvent.VK_LEFT:
                    int Min = Shape.getMinSquareX(shape);
                    if (Min > 0 && (flag == null || flag.equals("right is illegal"))) {
                        for (Square square : shape.getSquares()) {
                            square.getVector2D().addX(-1);
                        }
                        shape.setX(shape.getX() - 1);
                    }
                    break;
                case KeyEvent.VK_ENTER:
                    GameState.setPlay(true);
                    GameState.setShowNextShape(false);
                    GameState.setShowPressEnter(true);
                    new SoundPlayer("sounds////SFX_GameStart.mp3").start();
                    break;
                case KeyEvent.VK_TAB:
                    GameState.setBack(true);
                    new SoundPlayer("sounds////SFX_SpecialTetris.mp3").start();
                    break;
                case KeyEvent.VK_D:
                    if (shape.getType() == Constants.MOUNTAIN) {
                        if (CheckSpace.mountain(shape, shape.getPosition(), true))
                            shapePositionRight();
                    } else if (shape.getType() == Constants.RIGHT_FOOD) {
                        if (CheckSpace.rightFood(shape, shape.getPosition(), true))
                            shapePositionRight();
                    } else if (shape.getType() == Constants.LEFT_FOOD) {
                        if (CheckSpace.leftFood(shape, shape.getPosition(), true))
                            shapePositionRight();
                    } else if (shape.getType() == Constants.RIGHT_DUCK) {
                        if (CheckSpace.rightDuck(shape, shape.getPosition(), true)) {
                            if (shape.getPosition() == 1)
                                shape.setPosition(shape.getPosition() + 1);
                            else shape.setPosition(1);
                        }
                    } else if (shape.getType() == Constants.LEFT_DUCK) {
                        if (CheckSpace.leftDuck(shape, shape.getPosition(), true)) {
                            if (shape.getPosition() == 1)
                                shape.setPosition(shape.getPosition() + 1);
                            else shape.setPosition(1);
                        }
                    } else if (shape.getType() == Constants.WOOD) {

                        if (CheckSpace.wood(shape, shape.getPosition(), true)) {
                            if (shape.getPosition() == 1)
                                shape.setPosition(shape.getPosition() + 1);
                            else shape.setPosition(1);
                        }
                    }
                    break;
                case KeyEvent.VK_A:
                    if (shape.getType() == Constants.MOUNTAIN) {
                        if (CheckSpace.mountain(shape, shape.getPosition(), false))
                            shapePositionLeft();
                    } else if (shape.getType() == Constants.RIGHT_FOOD) {
                        if (CheckSpace.rightFood(shape, shape.getPosition(), false))
                            shapePositionLeft();
                    } else if (shape.getType() == Constants.LEFT_FOOD) {
                        if (CheckSpace.leftFood(shape, shape.getPosition(), false))
                            shapePositionLeft();
                    } else if (shape.getType() == Constants.RIGHT_DUCK) {
                        if (CheckSpace.rightDuck(shape, shape.getPosition(), false)) {
                            if (shape.getPosition() == 2)
                                shape.setPosition(shape.getPosition() - 1);
                            else shape.setPosition(2);
                        }
                    } else if (shape.getType() == Constants.LEFT_DUCK) {
                        if (CheckSpace.leftDuck(shape, shape.getPosition(), false)) {
                            if (shape.getPosition() == 2)
                                shape.setPosition(shape.getPosition() - 1);
                            else shape.setPosition(2);
                        }
                    } else if (shape.getType() == Constants.WOOD) {

                        if (CheckSpace.wood(shape, shape.getPosition(), false)) {
                            if (shape.getPosition() == 2)
                                shape.setPosition(shape.getPosition() - 1);
                            else shape.setPosition(2);
                        }
                    }
                    break;
                case KeyEvent.VK_ESCAPE:
                    try {
                        Data.saveGameState();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.exit(0);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    private void shapePositionRight() {
        if (shape.getPosition() != 4)
            shape.setPosition(shape.getPosition() + 1);
        else shape.setPosition(1);
    }

    private void shapePositionLeft() {
        if (shape.getPosition() != 1)
            shape.setPosition(shape.getPosition() - 1);
        else shape.setPosition(4);
    }
}
