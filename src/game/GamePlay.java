package game;

import entity.Shape;
import entity.Square;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import display.*;
import data.Data;
import constants.Constants;
import sounds.Sound;
import sounds.SoundPlayer;

public class GamePlay extends JComponent implements ActionListener {
    private Timer timer;
    private Drawer drawer;
    private Shape randomShape;
    private GameState gameState;
    private Shape nextShape;
    private Data data;

    public GamePlay() throws FileNotFoundException {
        data = new Data();
        new QuestionAtFirst();
        if (GameState.isPastGame())
            Data.setGameState();
        else Data.clearFiles();
        gameState = GameState.getInstance();
        nextShape = Shape.getRandomShape();
        randomShape = new Shape();
        randomShape = Shape.getRandomShape();
        drawer = new Drawer((Graphics2D) getGraphics());
        addKeyListener(new KeyboardListener(randomShape));
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(Constants.DELAY, this);
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;
        requestFocus();
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawer = new Drawer(g2D);
        if (GameState.isShowPressEnter()) {
            drawer.drawBackgroundBorder();
            drawer.drawBackground();
            drawer.drawBackgroundBorderNext();
            drawer.drawBackgroundNext();
            drawer.drawNextString();
            drawer.drawBackgroundBorderScore();
            drawer.drawBackgroundScore();
            drawer.drawScoreString();
            drawer.drawScoreNumber();
            drawer.drawBackgroundBorderBestScores();
            drawer.drawBackgroundBestScores();
            drawer.drawBestScoreString();
            try {
                drawer.drawBestScoresNumber(data.findTopTenScores());
            } catch (IOException e) {
                e.printStackTrace();
            }
            drawer.drawBackgroundBorderDeletedLines();
            drawer.drawBackgroundDeletedLines();
            drawer.drawNumberOfDeletedLine();
            drawer.drawShape(randomShape);
            drawer.drawGameState();
        }
        if (nextShape != null && GameState.isPlay())
            drawer.drawNextShape(nextShape);
        else if (!GameState.isPlay() && GameState.isShowNextShape() && GameState.isShowPressEnter())
            drawer.drawNextShape(randomShape);
        if (!GameState.isShowPressEnter()) {
            drawer.drawStartAndEnd("Press Enter to start");
        }
        if (GameState.isEndGame())
            drawer.drawStartAndEnd("Game Over!");
        g2D.dispose();
    }

    private void update() throws IOException {
        if (GameState.isPlay()) {
            checkCollisions();
            deleteRow();
            goBack();
            if (randomShape.isMove()) {
                for (Square square : randomShape.getSquares()) {
                    square.getVector2D().addY(1);
                }
                randomShape.setY(randomShape.getY() + 1);
            }
        }
    }

    private void checkCollisions() throws IOException {
        if (GameState.isPlay()) {
            int counter = 0;
            outer:
            for (Square square : gameState.getSquares()) {
                for (Square randomShapeSquare : randomShape.getSquares()) {
                    if (Square.getBox1(square).intersects(Square.getBox1(randomShapeSquare)) &&
                            square.getVector2D().getY() > randomShapeSquare.getVector2D().getY()) {
                        GameState.setScore(GameState.getScore() + 1);
                        new SoundPlayer("sounds////SFX_SpecialLineClearSingle.mp3").start();
                        randomShape.setMove(false);
                        for (Square shapeSquare : randomShape.getSquares()) {
                            gameState.addSquare(shapeSquare);
                        }
                        counter++;
                        if (Shape.checkPositionOfRandomShape(nextShape)) {
                            randomShape = nextShape;
                        } else {
                            GameState.setPlay(false);
                            data.SaveScore(GameState.getScore());
                            Data.clearFiles();
                            GameState.setEndGame(true);
                        }
                        addKeyListener(new KeyboardListener(randomShape));
                        break outer;
                    }
                }
            }
            if (counter == 1) nextShape = Shape.getRandomShape();
        }
    }

    private void goBack() {
        if (GameState.isBack()) {
            randomShape = new Shape(randomShape.getType(), randomShape.getColor(), 5, -2);
            randomShape.setMove(true);
            GameState.setPlay(true);
            GameState.setShowNextShape(true);
            addKeyListener(new KeyboardListener(randomShape));
            GameState.setBack(false);
        }
    }

    private void deleteRow() {
        for (int i = Constants.m - 1; i >= 0; i--) {
            int counter = 0;
            for (int j = 0; j < Constants.n; j++) {
                if (GameState.isExist(j, i)) {
                    counter++;
                } else break;
            }
            if (counter == Constants.n) {
                GameState.setDeletedLine(GameState.getDeletedLine() + 1);
                GameState.setScore(GameState.getScore() + 10);
                Sound.soundOfDeletedRow();
                for (int j = 0; j < Constants.n; j++) {
                    GameState.getInstance().removeSquare(GameState.getSquare(j, i));
                }
                for (int k = 0; k < i; k++) {
                    for (int j = 0; j < Constants.n; j++) {
                        if (GameState.isExist(j, k)) {
                            GameState.getSquare(j, k).getVector2D().addY(1);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        timer.start();
        repaint();
        try {
            update();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
